import React from 'react';
import phone from '../img/phone.png'

export default function footer() {
    return (
        <div style={{ height: 62, background: '#EAEEF6', textAlign: 'center', display: 'flex', position: "fixed", bottom: 0, width: '100%' }}>
            <div style={{ margin: 'auto' }}>
                <label className='footerLabel'>Copyright © 2020 </label><a href='infoplusvn.com'> infoplusvn.com. </a><label className='footerLabel'> All rights reserved</label></div>
            <div style={{ height: 74, width: 370, marginTop: -12, display: 'flex', position: "fixed", right: 0}}>
                <div>
                    <div id='phoneCurveAbove' />
                    <div id='phoneCurveBelow' />
                </div>
                <div style={{ background: '#DEBC65', width: 350, height: 74 }}>
                    <div style={{ display: 'flex' }}>
                        <div style={{ width: 40}}>
                            <img alt={'phoneImg'} style={{marginTop: 10 }} src={phone} />
                        </div>
                        <div>
                            <div style={{ display: 'flex', height: 45 }}>
                                <div className='phoneBlock'>
                                    <div>
                                        <label className='phoneLabel'>Trung tâm hỗ trợ</label>
                                    </div>
                                    <div>
                                        <label className='phoneNumber'>028 3825 7257</label>
                                    </div>
                                </div>
                                <div style={{border: '1px solid #C5A55A', height: 30, margin: 'auto'}}/>
                                <div className='phoneBlock'>
                                    <div>
                                        <label className='phoneLabel'>IT Support</label>
                                    </div>
                                    <div>
                                        <label className='phoneNumber'>024 7303 3322</label>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <label className='phoneLabel'>Working hour: 09:00 - 18:00</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}