import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import logo from '../img/top-logo.png';
import bell from '../img/bell.png';
import clock from '../img/clock.png';
import { withNamespaces } from 'react-i18next';
import i18n from '../i18n';
import MenuItem from './menu_bar/menuItem';

function MenuBar({t}) {
    const listMenu = ['Hệ thống', 'Quản lý chung', 'Quản lý phí', 'Cho thuê', 'Trang thiết bị', 'Ngân hàng', 'Khiếu nại'];
    const name = 'site_user';
    const listMenuDiv = listMenu.map((menu, index) => (
        <MenuItem key={index} title={menu} t={t}/>
    ))

    const changeLanguage = (lng) => {
        i18n.changeLanguage(lng);
    }

    return (
        <Navbar style={{ background: '#11284A', height: '56px' }} expand="lg">
            <Navbar.Brand href="#home">
                <img alt={'logoImg'} src={logo} />
            </Navbar.Brand>
            <Navbar.Text style={{ color: '#DEBC65' }}>
                CJ-8888
            </Navbar.Text>
            <Nav style={{margin: 'auto'}}>{listMenuDiv}</Nav>
            <Nav style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <NavDropdown title={name} id="basic-nav-dropdown" style={{marginRight: 10}}>
                    <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                    <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                </NavDropdown>
                <div style={{marginRight: 10}} onClick={() => changeLanguage('en')}>
                    <img alt={'bellImg'} src={bell} />
                </div>
                <div style={{marginRight: 10}} onClick={() => changeLanguage('vn')}>
                    <img alt={'clockImg'} src={clock} />
                </div>
            </Nav>
        </Navbar>)
}

export default withNamespaces()(MenuBar);