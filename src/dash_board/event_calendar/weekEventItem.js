import React from 'react';

export default function WeekEventItem(props) {
    const eventColor = new Map();
    eventColor.set('Birthday', { line: '#FF6A91', background: '#FFF7F9', font: '#ED5079' })
    eventColor.set('Booking', { line: '#C0A138', background: '#F8F5EB', font: '#BA971F' })
    eventColor.set('Contract', { line: '#0C5DE7', background: '#EDF3FB', font: '#0C5DE7' })
    eventColor.set('Complaint', { line: '#EE1313', background: '#FFEEF0', font: '#DC2626' })

    return (
        <div>
            {props.index <= 1 ? <div className='eventItemWeek' style={{ background: props.data.length !== 0 ? eventColor.get(props.data.eventType).background : '' }}>
                <div className='eventItemWeekLine' style={{ background: props.data.length !== 0 ? eventColor.get(props.data.eventType).line : '' }} />
                <div>
                    <label className='eventItemContent'>{props.data.content}</label>
                </div>
            </div>
            : <div></div>}
        </div>
    )
}