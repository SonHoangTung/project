import React from 'react';
import WeekEventItem from './weekEventItem';
import { DateTime } from 'luxon';

function convertListToMap(list) {
    let map = new Map();
    for (let i = 0; i < list.length; i++) {
        let day = list[i].date.day;
        if (map.get(day) != null)
            continue;
        const result = list.filter(obj => obj.date.day === day);
        map.set(day, result);
    }
    return map;
}

function createListEvent(size, map, startDate) {
    let mapEvent = new Map();
    for (let i = 0; i < size; i++) {
        let day = startDate.plus({ 'day': i }).day;
        if (map.get(day) == null)
            mapEvent.set(day, [[]]);
        else
            mapEvent.set(day, map.get(day));
    };

    return mapEvent;
}

function createArrListEvent(startDate, diffDate) {
    let list = [];
    for (let i = 0; i < diffDate; i++) {
        let date = startDate.plus({ 'day': i })
        list.push(
            { day: date, key: date.day }
        );
    }

    return list;
}

function convertNumberToDayOfWeek(number) {
    switch (number) {
        case 1: return 'Mon';
        case 2: return 'Tue';
        case 3: return 'Wed';
        case 4: return 'Thu';
        case 5: return 'Fri';
        case 6: return 'Sat';
        default: return 'Sun';
    }
}

export default function WeekMonthEvent(props) {
    const startDate = props.startDate;
    const endDate = props.endDate;
    const diffDate = endDate.day - startDate.day + 1;
    const day = DateTime.now().setZone("UTC+7").day;

    function redirectToDayTab(keyName, date) {
        props.onClickDay(keyName, date);
    }

    const eventArr = [
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Birthday', date: props.startDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Birthday', date: props.startDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Birthday', date: props.startDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Birthday', date: props.startDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Birthday', date: props.startDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Birthday', date: props.startDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Booking', date: props.endDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Contract', date: props.endDate },
        { content: 'Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS Hết hạn hợp đồng 003/2020/HDTS', eventType: 'Complaint', date: props.startDate },
    ]

    const listFilter = eventArr.filter(e => props.filter === 'All' || e.eventType === props.filter);

    const map = convertListToMap(listFilter);
    const eventMapList = createListEvent(diffDate, map, startDate);
    const listEvent = createArrListEvent(startDate, diffDate);

    const list = listEvent.map((event, key) => {
        return (
            <div key={key} onClick={() => redirectToDayTab('today', event.day)}>
                <div style={{height: 100, textAlign: "center", display: 'flex' }}>
                    <div className='eventItemWeekDay'>
                        <div>
                            <label className='dayOfWeekEvent' style={{ color: event.key === day ? '#225DC0' : '' }}>{convertNumberToDayOfWeek(event.day.weekday)}</label>
                        </div>
                        <div>
                            <label className='numberEvent' style={{ color: event.key === day ? '#225DC0' : '' }}>{event.key}</label>
                        </div>
                    </div>
                    <div>
                        {eventMapList.get(event.key)[0].length !== 0 ? eventMapList.get(event.key).map((v, k) => {
                            return (
                                <div key={k} >
                                    <WeekEventItem day={key} data={v} index={k} />
                                </div>
                            )
                        }) : <div>
                                <div className='eventItemLineNone' />
                            </div>}

                        {eventMapList.get(event.key).length > 2
                            ? <div className='overflowEventItem'>
                                <label>+ {eventMapList.get(event.key).length - 2} event</label>
                            </div>
                            : <div></div>}
                    </div>
                </div>
                <hr className='solid separateEventItem' />
            </div>
        )
    })

    return (
        <div className='contentTab'>
            <div>
                {list}
            </div>
        </div>
    )
}