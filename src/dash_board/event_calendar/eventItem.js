import React from 'react';

export default function eventItem(props) {
    const eventColor = new Map();
    eventColor.set('Birthday', { line: '#FF6A91', background: '#FFF7F9', font: '#ED5079' })
    eventColor.set('Booking', { line: '#C0A138', background: '#F8F5EB', font: '#BA971F' })
    eventColor.set('Contract', { line: '#0C5DE7', background: '#EDF3FB', font: '#0C5DE7' })
    eventColor.set('Complaint', { line: '#EE1313', background: '#FFEEF0', font: '#DC2626' })

    return (
        <div style={{ display: 'flex', marginTop: 20  }}>
            <div style={{ width: 60, height: 60, textAlign: "center" }}>
                <label className='numberEvent'>{props.number}</label>
            </div>
            <div style={{ height: 60, width: 380, display: 'flex'}}>
                <div style={{ height: 60, width: 3, background: eventColor.get(props.eventType).line }} />
                <div style={{ background: eventColor.get(props.eventType).background }} className='contentEventItem'>
                    <div>
                        <label className='contentEventLabel'>{props.eventContent.content}</label>
                    </div>
                    <div>
                        <label className='contentEventLabel' style={{ color: eventColor.get(props.eventType).font }}>From: {props.eventContent.begin} - To: {props.eventContent.end}</label>
                    </div>
                </div>
            </div>
        </div>
    )
}