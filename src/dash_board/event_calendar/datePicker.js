import React from 'react';
import vector from '../../img/vector.png';

function onChangeDateValue(props, valueStart, valueEnd) {
    props.onChangeDate(valueStart);
    if (props.type > 0) {
        props.onChangeEndDate(valueEnd)
    }
}

function getDayByType(type) {
    if (type === 0 || type === 2)
        return 1;
    else
        return 7;
}

export default function DatePicker(props) {
    const type = props.type;
    let startDate = props.startDate;
    const number = getDayByType(type);
    let endDate = props.endDate;

    return (
        <div>
            <div id='dateEvent'>
                <div style={{ margin: 'auto' }} className='vectorDate' onClick={() => {
                    startDate = startDate.minus(type === 2 ? { month: 1 } : { day: number });
                    endDate = startDate.plus(type === 2 ? { month: 1 } : { day: number }).minus({ day: 1 });
                    onChangeDateValue(props, startDate, endDate);
                }}>
                    <img alt={'vector-left'} style={{ margin: 'auto' }} src={vector} />
                </div>
                <label style={{ margin: 'auto', color: '#225DC0' }}>{startDate.toFormat('dd/MM/yyyy') + (type === 0 ? '' : (' - ' + endDate.toFormat('dd/MM/yyyy')))}</label>
                <div style={{ margin: 'auto' }} className='vectorDate' onClick={() => {
                    startDate = startDate.plus(type === 2 ? { month: 1 } : { day: number });
                    endDate = startDate.plus(type === 2 ? { month: 1 } : { day: number }).minus({ day: 1 });
                    onChangeDateValue(props, startDate, endDate);
                }}>
                    <img alt={'vector-right'} style={{ margin: 'auto', transform: 'rotate(180deg)' }} src={vector} />
                </div>
            </div>
        </div>
    )
}