import React from 'react';
import EventItem from './eventItem';

export default function DayEvent(props) {

    const eventArr = [{
        content: 'Expired Contract 003/2020/HDTS',
        begin: '00:00',
        end: props.date.toFormat('dd/MM/yyyy'),
        eventType: 'Birthday'
    },
    {
        content: 'Expired Contract 002/2020/HDTS',
        begin: '12:00',
        end: '23:59',
        eventType: 'Contract'
    },
    {
        content: 'Expired Contract 001/2020/HDTS',
        begin: '05:00',
        end: '23:59',
        eventType: 'Booking'
    },
    {
        content: 'Expired Contract 004/2020/HDTS',
        begin: '00:00',
        end: '15:00',
        eventType: 'Complaint'
    }]

    const listFilter = eventArr.filter(e => props.filter === 'All' || e.eventType === props.filter);

    const listEvent = listFilter.map((eventItem, index) => {
        return (
            <div key={index}>
                <EventItem number={index + 1} eventType={eventItem.eventType} eventContent={eventItem} />
            </div>
        )
    })

    return (
        <div>
            {listEvent}
        </div>
    )
}