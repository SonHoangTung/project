import React from 'react';
import StatusItem from './statusItem';
import paid from '../../img/paid.png'
import unpaid from '../../img/unpaid.png'
import overpaid from '../../img/overpaid.png'


export default function status(props) {
    const value = props.value;
    return (
        <div>
            <div style={{marginBottom: 25}}>
                <label>{props.title}</label>
            </div>
            <div id='status'>
                <div>
                    <StatusItem name={'paid'} img={paid} data={value.paid} />
                </div>
                <hr className='solid'/>
                <div>
                    <StatusItem name={'unpaid'} img={unpaid} data={value.unpaid} />
                </div>
                <hr className='solid'/>
                <div>
                    <StatusItem name={'overpaid'} img={overpaid} data={value.overpaid} />
                </div>
            </div>
        </div>

    )
}