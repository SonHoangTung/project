import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default function statusItem(props) {
    const formatter = new Intl.NumberFormat('de-DE', {
        style: 'currency',
        currency: 'VND'
    })
    const total = formatter.format(props.data.total);
    return (
        <Container style={{ height: 50, marginBottom: 10 }}>
            <Row>
                <Col xs='2' style={{ display: 'flex', alignItems: 'center' }}>
                    <img alt={''} src={props.img} />
                </Col>
                <Col>
                    <div>
                        <div>
                            <label>{props.name}</label>
                        </div>
                        <div style={{ display: 'flex' }}>
                            <div>
                                <label>{props.data.room} room</label>
                            </div>
                            <div style={{ marginLeft: 'auto' }}>
                                <label>{total}</label>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    )
}