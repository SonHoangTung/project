import React from 'react';
import Chart from 'react-google-charts';

export default function rightMain() {
    return (
        <div style={{ display: 'flex', maxWidth: 900 }}>
            <Chart
                chartType="LineChart"
                width={'100%'}
                height={300}
                loader={<div>Loading Chart</div>}
                data={[
                    ['x', 'answer', 'complaint'],
                    [0, 8, 5],
                    [1, 10, 5],
                    [2, 23, 15],
                    [3, 17, 9],
                    [4, 18, 10],
                    [5, 9, 5],
                    [6, 11, 3],
                    [7, 27, 19],
                    [8, 27, 19],
                    [9, 27, 19],
                    [10, 27, 19],
                ]}
                options={{
                    colors: ['#25AA7A', '#DE390B'],
                    vAxis: {
                        title: 'Values',
                    },
                    legend: {position: 'bottom'},
                    chartArea: {
                        height: '75%',
                        width: '90%'
                    },
                    width: 'auto',
                    height: 'auto',
                }}
                rootProps={{ 'data-testid': '2' }}
            />
        </div>
    )
}