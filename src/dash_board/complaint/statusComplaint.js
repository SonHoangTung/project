import React, { useEffect, useState } from 'react';
import StatusItem from './statusItem';
import api from '../../util/api';
import { UserContext } from '../../app';

export default function StatusComplaint() {
    const [state, dispatch] = React.useContext(UserContext)
    
    const [statusComplaint, setStatusComplaint] = useState({new: 0, overdue: 0, inprocess: 0, complete: 0})

    useEffect(() => {
        api.get('http://localhost:4000/dashboard/statusComplaint' , {aptId: state.site, userId: 'thai1'}).then(result => {
            const value = result[0]
            setStatusComplaint({
                new: value.TOTAL_NEW_COMPLAINT,
                overdue: value.TOTAL_OVER_COMPLAINT,
                inprocess: value.TOTAL_PROCESSING_COMPLAINT,
                complete: value.TOTAL_DONE_COMPLAINT
            })
        }).then(err => {
            console.log(err);
        })
    }, [state.site])
    return (
        <div style={{margin: 5}}>
            <div className='complaintStatus'>
                <div>
                    <StatusItem color={'#CFEDF0'} status={'New'} number={statusComplaint.new} contentColor={'#128E9F'}/>
                </div>
                <div>
                    <StatusItem color={'#FFE3E7'} status={'Overdue'} number={statusComplaint.overdue} contentColor={'#D00A19'}/>
                </div>
            </div>
            <div className='complaintStatus'>
                <div>
                    <StatusItem color={'#CAF4ED'} status={'In Process'} number={statusComplaint.inprocess} contentColor={'#05735F'}/>
                </div>
                <div>
                    <StatusItem color={'#EDF3FB'} status={'Complete'} number={statusComplaint.complete} contentColor={'#225DC0'}/>
                </div>
            </div>
        </div>
    )
}