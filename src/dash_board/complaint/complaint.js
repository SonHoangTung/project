import React from 'react';
import Chart from './chart';
import StatusComplaint from './statusComplaint';
import { Button, Container, Row, Col } from 'react-bootstrap';

export default function complaint() {
    return (
        <div style={{ padding: '10px 10px 10px 0' }}>
            <div style={{ display: 'flex' }}>
                <div style={{ marginRight: 10 }}>
                    <div style={{ background: '#225DC0', borderRadius: '0 4px 4px 0', height: 35, width: 3 }} />
                </div>
                <div>
                    <div>
                        <label style={{ marginBottom: 0 }} className='title'>Complaint</label>
                    </div>
                    <div>
                        <label style={{ color: '#9B9C9F' }}>Status of complaint</label>
                    </div>
                </div>
                <div style={{ marginLeft: 'auto', marginTop: 10 }}>
                    <Button style={{ background: 'transparent', color: '#899ED6', borderColor: '#899ED6', fontSize: 14}}>Go to complaint</Button>
                </div>
            </div>
            <div style={{ height: 300 }}>
                <Container style={{ height: '100%' }}>
                    <Row style={{ height: '100%' }}>
                        <Col xs={8}>
                            <div style={{ height: '100%' }}>
                                <Chart />
                            </div>
                        </Col>
                        <Col xs={4} style={{ padding: 0 }}>
                            <div>
                                <StatusComplaint />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}