import React from 'react';

export default function statusItem(props) {
    return (
        <div style={{ background: props.color, borderRadius: 10, width: '13vh', height: '12vh', margin: 5, textAlign: 'center'}}>
            <div style={{display: 'flex', justifyContent: 'center', flexDirection: 'column', height: '100%'}}>
                <div>
                    <label style={{ color: props.contentColor }}>{props.status}</label>
                </div>
                <div>
                    <label style={{ color: props.contentColor }} className='complaintLabel'>{props.number}</label>
                </div>
            </div>
        </div>
    )
}