import React, { useEffect, useState } from 'react';
import PieChart from './pieChart';
import { Container, Row, Col } from 'react-bootstrap';
import { UserContext } from '../../app';
import api from '../../util/api'

export default function Room() {
    const [state, dispatch] = React.useContext(UserContext)
    const [pieChart, setPieChart] = useState({
        resident: { used: 0, unUsed: 0 },
        office: { used: 0, unUsed: 0 }
    })

    useEffect(() => {
        api.get('http://localhost:4000/dashboard/roomStatus0', { aptId: state.site, userId: 'thai1' }).then(value => {

            const result = value[0];
            console.log('========================', result);
            setPieChart({
                resident: {used: result.NUM_RES_USE, unUsed: result.NUM_RES_VC},
                office: {used: result.NUM_OFFICE_USE, unUsed: result.NUM_OFFICE_VC}
            })

        }).catch(error => {
            console.log(error);
        })
    }, [state.site])


    return (
        <div className='centerDiv'>
            <div style={{ display: 'flex', marginBottom: 10, paddingTop: 10 }}>
                <div style={{ marginRight: 10 }}>
                    <div style={{ background: '#225DC0', borderRadius: '0 4px 4px 0', height: 35, width: 3 }} />
                </div>
                <div>
                    <label style={{ marginBottom: 0 }} className='title'>Room</label>
                </div>
                <div style={{ marginLeft: 'auto', display: 'flex' }}>
                    <div className='legendItem'>
                        <div className='legendPieChart' style={{ background: '#25AA7A' }} />
                        <div style={{ margin: 'auto' }}>
                            <label style={{ marginBottom: 0 }}>Đã thuê</label>
                        </div>
                    </div>
                    <div className='legendItem'>
                        <div className='legendPieChart' style={{ background: '#DE390B' }} />
                        <div style={{ margin: 'auto' }}>
                            <label style={{ marginBottom: 0 }}>Còn trống</label>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{ height: 250 }}>
                <Container>
                    <Row>
                        <Col>
                            <PieChart name={'Resident'} value={[pieChart.resident.used, pieChart.resident.unUsed]} />
                        </Col>
                        <Col>
                            <PieChart name={'Office'} value={[pieChart.office.used, pieChart.office.unUsed]} />
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}