import React from 'react'
import Chart from 'react-google-charts'


export default function pieChart(props) {

    return (
        <div>
            <Chart
                chartType="PieChart"
                loader={<div>Loading Chart</div>}
                data={[
                    ['Vacancy', 'number of room'],
                    ['Còn trống', props.value[0]],
                    ['Đã thuê', props.value[1]] // Below limit.
                ]}
                formatters={[

                ]}
                options={{
                    colors: ['#DE390B', '#25AA7A'],
                    legend: 'none',
                    width: 'auto',
                    height: 'auto',
                    fontSize: 14,
                    chartArea: {
                        height: '90%',
                        width: '90%'
                    },
                    tooltip: { trigger: 'none' },
                    pieSliceText: 'value'
                }}
            />
            <div style={{textAlign: 'center'}}>
                <label className='pieChartTitle'>{props.name}</label>
            </div>
        </div>
    )
}