import React, { useState } from 'react';
import { Tab, Nav } from 'react-bootstrap';
import DayEvent from '../event_calendar/dayEvent';
import WeekMonthEvent from '../event_calendar/weekMonthEvent';
import DatePicker from '../event_calendar/datePicker';
import { DateTime } from 'luxon';
import { UserContext } from '../../app';

function getEndDateByType(type, date) {
    if (type === 0)
        return '';
    else if (type === 1)
        return date.plus({ day: 6 });
    else
        return date.plus({ month: 1 }).minus({ day: 1 });
}

function getStartDateByType(type, date) {
    if (type === 0)
        return date;
    else if (type === 1) {
        const startDay = date.day - date.weekday + 1;
        return DateTime.local(date.year, date.month, startDay, 0, 0, 0, 0);
    } else {
        return DateTime.local(date.year, date.month, 1, 0, 0, 0, 0);
    }
}

export default function RightMain() {
    const [state, dispatch] = React.useContext(UserContext)
    const dt = DateTime.now().setZone("UTC+7");
    const [dayDate, setDayDate] = useState(getStartDateByType(0, dt));
    const [weekDate, setWeekDate] = useState(getStartDateByType(1, dt));
    const [monthDate, setMonthDate] = useState(getStartDateByType(2, dt));
    const [weekEndDate, setWeekEndDate] = useState(getEndDateByType(1, weekDate));
    const [monthEndDate, setMonthEndDate] = useState(getEndDateByType(2, monthDate));
    const [selectedTab, setSelectedTab] = useState('today');
    const [selectedType, setSeletedType] = useState('All')

    function onClickDay(keyName, dayDate) {
        setSelectedTab(keyName);
        setDayDate(dayDate);
    }

    function onChangeValue(e) {
        setSeletedType(e.target.value);
        console.log('==================', e.target.value);
    }

    return (
        <div className="split" id='rightMain'>
            <div style={{ display: 'flex', padding: '15px 5px 5px 0' }}>
                <div style={{ background: '#225DC0', borderRadius: '0 4px 4px 0', height: 35, width: 3 }} />
                <div style={{ marginLeft: 10 }}>
                    <label>{state.value}</label>
                    <label className='title'>Event calendar</label>
                </div>
                <div style={{ margin: 'auto', marginRight: 0 }}>
                    <select defaultValue='All' className='selectedList' style={{ background: '#EAEEF6', }} onChange={(value) => onChangeValue(value)}>
                        <option value="All">All</option>
                        <option value="Birthday">Birthday</option>
                        <option value="Contract">Contract</option>
                        <option value="Booking">Booking</option>
                        <option value="Complaint">Complaint</option>
                    </select>
                </div>
            </div>
            <Tab.Container id="left-tabs-example" defaultActiveKey="today" activeKey={selectedTab}>
                <div style={{ background: '#EDF3FB', borderRadius: 6, margin: 5, padding: 5 }}>
                    <Nav variant="pills" style={{ display: 'flex', justifyContent: 'space-around' }}>
                        <Nav.Item onClick={() => setSelectedTab('today')} className='calendarDate'>
                            <Nav.Link eventKey="today">Today</Nav.Link>
                        </Nav.Item>
                        <Nav.Item onClick={() => setSelectedTab('week')} className='calendarDate'>
                            <Nav.Link eventKey="week">Week</Nav.Link>
                        </Nav.Item>
                        <Nav.Item onClick={() => setSelectedTab('month')} className='calendarDate'>
                            <Nav.Link eventKey="month">Month</Nav.Link>
                        </Nav.Item>
                    </Nav>
                </div>
                <Tab.Content className='rightContent'>
                    <Tab.Pane eventKey="today">
                        <DatePicker type={0} number={0} startDate={dayDate} endDate={dayDate} onChangeDate={setDayDate} />
                        <DayEvent date={dayDate} endDate={dayDate} filter={selectedType} />
                    </Tab.Pane>
                    <Tab.Pane eventKey="week">
                        <DatePicker type={1} number={6} onChangeDate={setWeekDate} startDate={weekDate} endDate={weekEndDate} onChangeEndDate={setWeekEndDate} />
                        <WeekMonthEvent startDate={weekDate} endDate={weekEndDate} onClickDay={onClickDay} filter={selectedType} />
                    </Tab.Pane>
                    <Tab.Pane eventKey="month">
                        <DatePicker type={2} number={1} onChangeDate={setMonthDate} startDate={monthDate} endDate={monthEndDate} onChangeEndDate={setMonthEndDate} />
                        <WeekMonthEvent startDate={monthDate} endDate={monthEndDate} onClickDay={onClickDay} filter={selectedType} />
                    </Tab.Pane>
                </Tab.Content>
            </Tab.Container>
        </div>
    )
}