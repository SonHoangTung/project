import React from 'react';
import { Tab, Tabs, Container, Row, Col } from 'react-bootstrap';
import LeftMain from './leftMain';
import RightMain from './rightMain';
import CenterMain from './centerMain';

function mainDashBoard() {
    return (
        <Tabs defaultActiveKey="home" id="uncontrolled-tab-example" style={{backgroundColor: '#DEBC65'}}>
            <Tab eventKey="home" title="Home">
                <Container style={{maxWidth: '100%', backgroundColor: '#F1F5F9', paddingTop: 20, paddingBottom: 100}}>
                    <Row>
                        <Col xs={2}>
                            <LeftMain />
                        </Col>
                        <Col xs={7}>
                            <CenterMain />
                        </Col>
                        <Col xs={3}>
                            <RightMain />
                        </Col>
                    </Row>
                </Container>
            </Tab>
        </Tabs>
    )
}

export default mainDashBoard;
