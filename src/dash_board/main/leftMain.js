import React, { useEffect, useState } from 'react';
import Status from '../statusOfReceipt/staus'
import api from '../../util/api';
import { UserContext } from '../../app';

export default function LeftMain() {
    const [selectedSite, setSelectedSite] = useState(0)
    const [state, dispatch] = React.useContext(UserContext)

    const [resident, setResident] = useState({
        paid: { total: 0, room: 0 },
        unpaid: { total: 0, room: 0 },
        overpaid: { total: 0, room: 0 }
    })

    const [office, setOffice] = useState({
        paid: { total: 0, room: 0 },
        unpaid: { total: 0, room: 0 },
        overpaid: { total: 0, room: 0 }
    })

    useEffect(() => {
        getData(selectedSite);
    }, [selectedSite])

    const getData = (site) => {
        api.get('http://localhost:4000/dashboard/residentPaymentMF0', { aptId: site, userId: 'thai1' }).then(value => {

            const result = value[0];
            setResident({
                paid: { total: result.AMOUNT_TOTAL, room: result.TOTAL_PAID },
                unpaid: { total: result.AMOUNT_TOTAL_UNPAY, room: result.TOTAL_UNPAY },
                overpaid: { total: result.TOTAL_AMOUNT_OUTDATE, room: result.TOTAL_OUTDATE }
            });
        }).catch(error => {
            console.log(error);
        })

        api.get('http://localhost:4000/dashboard/officePayment', { aptId: site, userId: 'thai1' }).then(value => {
            const result = value[0];
            setOffice({
                paid: { total: result.OFF_TOTAL_PAID, room: result.TOTAL_PAID },
                unpaid: { total: result.OFF_TOTAL_UNPAID, room: result.TOTAL_UNPAID },
                overpaid: { total: result.OFF_TOTAL_UNPAID_OVERDATE, room: result.TOTAL_OVERDATE }
            });

        }).catch(error => {
            console.log(error);
        })
    }

    const changeSite = (site) => {
        dispatch({type: "change_site", value: site.target.value})
        console.log(site.target.value);
        setSelectedSite(site.target.value)
        // getData(site.target.value);
    }

    return (
        <div className="split" id='left'>
            <div style={{ marginBottom: 5 }}>
                <select defaultValue='Goldmark City' className='selectedList' onChange={(value) => changeSite(value)}>
                    <option value="0">All</option>
                    <option value="1111">Goldmark City</option>
                    <option value="1212">Vinhomes Royal City</option>
                    <option value="5512">Vinhomes SkyLake</option>
                    <option value="8888">FLC Twin Tower</option>
                </select>
            </div>

            <div id='appLabel'>
                <div>
                    <label style={{ margin: "auto" }} value='Tổng số cư dân dùng App'>Số cư dân dùng App</label>
                </div>
                <div style={{ marginLeft: "auto" }}>
                    <label style={{ margin: "auto" }} value='200/300'>200/300</label>
                </div>
            </div>

            <div style={{ borderRadius: 10, background: '#FFFFFF', padding: '0 5px 5px 5px', height: '85%' }}>
                <div>
                    <div style={{ background: '#225DC0', borderRadius: '0 0 4px 4px', height: 3, width: 60, margin: 'auto' }} />
                </div>
                <div style={{ textAlign: 'center', marginBottom: 29 }} >
                    <label value="Status of Receipt" className='title'>Status of Receipt</label>
                </div>
                <div>
                    <div style={{ height: 250, marginBottom: 25 }}>
                        <Status title={'RESIDENT'} value={resident} />
                    </div>
                    <div style={{ height: 250 }}>
                        <Status title={'OFFICE'} value={office} />
                    </div>
                </div>
            </div>
        </div>
    )
}