import React from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import Complaint from '../complaint/complaint';
import Room from '../room/room';
import Booking from '../booking/booking';

export default function centerMain() {
    return (
        <div className="split right">
            <div style={{background: '#FFFFFF', borderRadius: 10, height: 400, marginBottom: 10}}>
                <Complaint />
            </div>
            <div>
                <Container>
                    <Row>
                        <Col className="centerBottom">
                            <Booking />
                        </Col>
                        <Col className="centerBottom">
                            <Room />
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}