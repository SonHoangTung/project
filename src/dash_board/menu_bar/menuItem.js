import React, { useState } from 'react';
import { Nav, NavDropdown } from 'react-bootstrap';
import { withNamespaces } from 'react-i18next';

function MenuItem(props) {
    const [show, setShow] = useState(false)
    const title = props.title;
    const t = props.t;

    const showDropdown = (e) => {
        setShow(!show);
    }
    const hideDropdown = e => {
        setShow(false);
    }

    return (
        <Nav>
            <NavDropdown title={title} id="basic-nav-dropdown" onMouseEnter={showDropdown} onMouseLeave={hideDropdown} show={show}>
                <NavDropdown.Item href="#action/3.1">{t('Action')}</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
            </NavDropdown>
        </Nav>
    )

}

export default withNamespaces()(MenuItem);