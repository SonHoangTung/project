import React, { useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import api from '../../util/api';
import { UserContext } from '../../app';

export default function Booking() {
    const [state, dispatch] = React.useContext(UserContext)

    const [booking, setBooking] = useState({
        total: { quantity: 0, price: 0, deposit: 0 },
        unConfirmed: { quantity: 0, price: 0, deposit: 0 },
        confirmed: { quantity: 0, price: 0, deposit: 0 }
    })

    useEffect(() => {
        api.get('http://localhost:4000/dashboard/allBooking', { aptId: state.site, userId: 'thai1' }).then(value => {

            const result = value[0];
            setBooking({
                total: { quantity: result.TOTAL_QTY, price: result.TOTAL_AMOUNT, deposit: result.TOTAL_DEPOSIT },
                unConfirmed: { quantity: result.TOTAL_QTY_NOT, price: result.TOTAL_AMOUNT_NOT, deposit: result.TOTAL_DEPOSIT_NOT },
                confirmed: { quantity: result.TOTAL_QTY_CONFIRMED, price: result.TOTAL_AMOUNT_CONFIRMED, deposit: result.TOTAL_DEPOSIT_CONFIRMED }
            })

        }).catch(error => {
            console.log(error);
        })
    }, [state.site])

    return (
        <div className='centerDiv'>
            <div style={{ display: 'flex', marginBottom: 10, paddingTop: 10 }}>
                <div style={{ marginRight: 10 }}>
                    <div style={{ background: '#225DC0', borderRadius: '0 4px 4px 0', height: 35, width: 3 }} />
                </div>
                <div>
                    <label style={{ marginBottom: 0 }} className='title'>Booking</label>
                </div>
            </div>
            <div style={{ padding: 5 }} className="centerDiv">
                <Table reponsive="sm" style={{ margin: 'auto' }}>
                    <thead>
                        <tr>
                            <th className='bookingLabel'>#</th>
                            <th className='bookingLabel'>QTT</th>
                            <th className='bookingLabel'>Price</th>
                            <th className='bookingLabel'>Deposit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className='bookingLabel'>Tổng</td>
                            <td><label className='numberBookingLabel'>{booking.total.quantity}</label></td>
                            <td><label className='numberBookingLabel'>{booking.total.price}</label></td>
                            <td><label className='numberBookingLabel'>{booking.total.deposit}</label></td>
                        </tr>
                        <tr>
                            <td className='bookingLabel'>Chưa xác nhận</td>
                            <td><label className='numberBookingLabel'>{booking.unConfirmed.quantity}</label></td>
                            <td><label className='numberBookingLabel'>{booking.unConfirmed.price}</label></td>
                            <td><label className='numberBookingLabel'>{booking.unConfirmed.deposit}</label></td>
                        </tr>
                        <tr>
                            <td className='bookingLabel'>Đã xác nhận</td>
                            <td><label className='numberBookingLabel'>{booking.confirmed.quantity}</label></td>
                            <td><label className='numberBookingLabel'>{booking.confirmed.price}</label></td>
                            <td><label className='numberBookingLabel'>{booking.confirmed.deposit}</label></td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        </div>
    )
}