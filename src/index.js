import React from 'react';
import ReactDOM from 'react-dom';
import MenuBar from './dash_board/menuBar';
import MainDashBoard from './dash_board/main/mainDashBoard';
import Footer from './dash_board/footer';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import './i18n';
import { UserProvider } from './app';

class Main extends React.Component {
    render() {
        return (
            <UserProvider>
                <div style={{ height: '100vh' }}>
                    <MenuBar />
                    <MainDashBoard />
                    <Footer />
                </div>
            </UserProvider>
        )
    }
}

// ========================================

ReactDOM.render(
    <Main />,
    document.getElementById('root')
);
