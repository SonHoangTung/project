import axios from 'axios';

function get(url, params) {
    const promise = axios.get(url, { params: params })
    const dataPromise = promise.then((response) => response.data)
    return dataPromise
}

function post(url, data, params) {
    const promise = axios.post(url, { data: data }, { params: params })
    const dataPromise = promise.then((response) => response.data)
    return dataPromise
}

function put(url, data, params) {
    const promise = axios.put(url, { data: data }, { params: params })
    const dataPromise = promise.then((response) => response.data)
    return dataPromise
}

function remove (url, params) {
    const promise = axios.delete(url, { params: params })
    const dataPromise = promise.then((response) => response.data)
    return dataPromise
}

const api = {
    get, post, put, remove
}

export default api