export const reducer = (state, action) => {
    switch (action.type) {
        case "change_site":
            return {
                ...state,
                site: action.value
            }

        default:
            return state
    }
}

export const initialState = {
    site: 0
}