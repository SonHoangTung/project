exports.colorMapDash = function () {
    const eventColor = new Map();
    eventColor.set('Birthday', { line: '#FF6A91', background: '#FFF7F9', font: '#ED5079' })
    eventColor.set('Booking', { line: '#C0A138', background: '#F8F5EB', font: '#BA971F' })
    eventColor.set('Contract', { line: '#0C5DE7', background: '#EDF3FB', font: '#0C5DE7' })
    eventColor.set('Complaint', { line: '#EE1313', background: '#FFEEF0', font: '#DC2626' })
    return eventColor;
}