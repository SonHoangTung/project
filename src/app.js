import React from 'react'
import {reducer, initialState} from './util/reducer'

export const UserContext = React.createContext({
    state: initialState,
    dispatch: () => {}
})

export const UserProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(reducer, initialState)

    return (
        <UserContext.Provider value={[state, dispatch]}>
            {children}
        </UserContext.Provider> 
    )
}